pipeline {
	agent {label 'aws'}
    environment {
        XDEBUG_MODE='coverage'
        MODES='coverage'
    }
	stages {
// 		stage ('test') {
// 			steps {
// 				sh '''
// 				echo "Composer install..."
// 				composer install
// 				echo "Running tests..."
//                 phpunit tests
// 				'''
// 				script {
//                 					CI_ERROR = "Failed while deploying application"
//                 					}
// 			}
// 		}
        stage('PHP 8.1') {
          agent {
            docker {
              image 'php:8.1.0-fpm'
              args '-u root:sudo'
            }

          }
          steps {
            echo 'Running PHP 8.1 tests...'
            sh 'php -v'

            sh 'cp php.ini /usr/local/etc/php/'

            echo 'Installing Dependencies...'
            sh '''
                apt-get update
                apt-get install bash zip unzip curl
                pecl install xdebug \
                && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
                && echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
                && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/xdebug.ini \
                && echo "xdebug.client_port=9000" >> /usr/local/etc/php/conf.d/xdebug.ini \
                && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini \
                && echo "xdebug.mode=develop,coverage,debug" >> /usr/local/etc/php/conf.d/xdebug.ini
            '''

            echo 'Installing Composer...'
            sh 'curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer'

            echo 'Installing project composer dependencies...'
            sh 'cd $WORKSPACE && composer install'

            echo 'Running PHPUnit tests...'
             catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                sh '''
                 ./vendor/bin/phpunit \
                 --coverage-html ./reports/coverage \
                 --coverage-clover ./reports/coverage/coverage.xml \
                 --log-junit ./reports/unitreport.xml
                '''
            }

            echo currentBuild.result

            sh 'chmod -R a+w $PWD && chmod -R a+w $WORKSPACE'
            echo 'Building JUnit report...'
            junit 'reports/*.xml'

script {
            if(currentBuild.result == 'FAILURE') {
                error('Tests failed')
            }
            }
          }
        }
        stage('After Tests') {
            steps {
                sh """
                echo 'After Tests Failed'
                echo ${currentBuild.currentResult}
                echo ${currentBuild.result}
                """
            }
        }
	}

	post {
		always {
			script {
				CONSOLE_LOG = "${env.BUILD_URL}/console"
				BUILD_STATUS = currentBuild.currentResult
				if (currentBuild.currentResult == 'SUCCESS') {
					CI_ERROR = "NA"
                }
            }
            publishHTML(target:[
                allowMissing: true,
                alwaysLinkToLastBuild: true,
                keepAll: true,
                reportDir: "${WORKSPACE}/reports/coverage",
                reportFiles: 'report.html',
                reportName: 'CI-Build-HTML-Report',
                reportTitles: 'CI-Build-HTML-Report'
                ])
            sendSlackNotifcation()
		}
	}
}

def sendSlackNotifcation()
{
	if ( currentBuild.currentResult == "SUCCESS" ) {
		buildSummary = "Job:  ${env.JOB_NAME}\n Status: *SUCCESS*\n Build Report: ${env.BUILD_URL}CI-Build-HTML-Report"

		slackSend color : "good", message: "${buildSummary}", channel: '#digital-dealership-cdci'
		}
	else {
		buildSummary = "Job:  ${env.JOB_NAME}\n Status: *FAILURE*\n Error description:  \nBuild Report :${env.BUILD_URL}CI-Build-HTML-Report"
		slackSend color : "danger", message: "${buildSummary}", channel: '#digital-dealership-cdci'
		}
}