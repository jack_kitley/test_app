<?php

class User
{
    public $firstName;

    public $lastName;

    public $email;

    protected $mailer;

    /**
     * @param mixed $mailer
     */
    public function setMailer(Mailer $mailer): void
    {
        $this->mailer = $mailer;
    }

    public function getFullName()
    {
        return trim($this->firstName.' '.$this->lastName);
    }

    public function notify($message)
    {
        return $this->mailer->sendMail($this->email,$message);
    }
}