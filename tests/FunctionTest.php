<?php
use PHPUnit\Framework\TestCase;

class FunctionTest extends TestCase
{
    public function testAddReturnsTheCorrectSum()
    {
        require 'Functions.php';

        $functions = new Functions();

        $this->assertEquals(4, $functions->add(2, 2));
        $this->assertEquals(8, $functions->add(4, 4));
    }

    public function testAddDoesNotReturnTheIncorrectSum()
    {
        $functions = new Functions();

        $this->assertNotEquals(5,$functions->add(3,4));
    }
}