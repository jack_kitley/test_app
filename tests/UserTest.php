<?php

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testReturnsFullName()
    {
        $user = new User;
        $user->firstName = 'Jack';
        $user->lastName = 'Kitley';

        $this->assertEquals('Jack Kitley',$user->getFullName());
    }

    public function testFullNameIsEmptyByDefault()
    {
        $user = new User;

        $this->assertEquals('',$user->getFullName());
    }

    public function testNotificationsIsSent()
    {
        $user = new User;
        $user->email = 'jack@overtake.co.za';

        $mockMailer = $this->createMock(Mailer::class);
        $mockMailer->expects($this->exactly(1))
            ->method('sendMail')
            ->with($this->equalTo('jack@overtake.co.za'),$this->equalTo('Hello'))
            ->willReturn(true);

        $user->setMailer($mockMailer);

        $this->assertTrue($user->notify("Hello"));
    }

    public function testCannotNotifyUserWithNoEmail()
    {
        $user = new User;
        $mockMailer = $this->getMockBuilder(Mailer::class)
            ->setMethods(null)
            ->getMock();

        $user->setMailer($mockMailer);

        $this->expectException(Exception::class);

        $user->notify("Hello");
    }

}